package com.app.api;

import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.*;
import lombok.extern.slf4j.Slf4j;

/**
 * 可以删除这个controller，将静态资源部署到servlet并不是最佳实践，毕竟 nginx 对资源占用极低
 */
@ApiIgnore
@Controller
public class MainController {
    @RequestMapping("/")
    public String index() {
        return "index.html";
    }
}
