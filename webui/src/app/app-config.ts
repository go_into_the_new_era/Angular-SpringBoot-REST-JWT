import { Injectable } from '@angular/core';

/**
 * This is a singleton class
 */
@Injectable()
export class AppConfig {
    //Provide all the Application Configs here
    version: string = '1.0.0';
    locale: string = 'en-US';
    currencyFormat = { style: 'currency', currency: 'USD' };
    dateFormat = { year: 'numeric', month: 'short', day: 'numeric' };

    // API Related configs
    apiPort: string = '9119';
    apiProtocol: string;
    apiHostName: string;
    baseApiPath: string;

    constructor() {
        if (this.apiProtocol === undefined) {
            this.apiProtocol = window.location.protocol;
        }
        if (this.apiHostName === undefined) {
            this.apiHostName = window.location.hostname;
        }
        if (this.apiPort === undefined) {
            this.apiPort = window.location.port;
        }
        if (this.apiHostName.includes('infomud') || this.apiHostName.includes('heroku')) {
            this.baseApiPath = this.apiProtocol + '//' + this.apiHostName + '/';
        } else {
            this.baseApiPath = this.apiProtocol + '//' + this.apiHostName + ':' + this.apiPort + '/';
        }
        if (this.locale === undefined) {
            this.locale = navigator.language;
        }
    }
}
