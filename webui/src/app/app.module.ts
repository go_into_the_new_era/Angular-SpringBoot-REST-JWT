
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//Third Party Modules
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ClarityModule } from '@clr/angular';

//Local App Modules
import { AppRoutingModule } from './app-routing.module';

// Directives
import { TrackScrollDirective } from './directives/track_scroll/track_scroll.directive';

// Components
import { BadgeComponent } from './components/badge/badge.component';
import { LegendComponent } from './components/legend/legend.component';
import { LogoComponent } from './components/logo/logo.component';

//Pages  -- Pages too are components, they contain other components
import { AppComponent } from './app.component';
import { HomeComponent } from './home.component';
import {
  LoginComponent, LogoutComponent, DashboardComponent, OrderStatsComponent, ProductStatsComponent
  , ProductsComponent, CustomersComponent, OrdersComponent, OrderDetailsComponent, EmployeesComponent
} from './pages';
// Services
import { AppConfig } from './app-config';
import {
  UserInfoService, AuthGuard, ApiRequestService, TranslateService, LoginService
  , OrderService, ProductService, CustomerService, EmployeeService
} from './services';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    // Thirdparty Module
    NgxDatatableModule,
    NgxChartsModule,
    ClarityModule.forChild(),
    // Local App Modules
    AppRoutingModule
  ],

  declarations: [
    // Components
    BadgeComponent,
    LegendComponent,
    LogoComponent,

    //Pages -- Pages too are components, they contain other components
    AppComponent,
    HomeComponent,
    LoginComponent,
    LogoutComponent,
    DashboardComponent,
    ProductStatsComponent,
    OrderStatsComponent,
    ProductsComponent,
    EmployeesComponent,
    CustomersComponent,
    OrdersComponent,
    OrderDetailsComponent,

    //Directives
    TrackScrollDirective
  ],

  providers: [
    AuthGuard,
    UserInfoService,
    TranslateService,
    ApiRequestService,
    LoginService,
    OrderService,
    ProductService,
    CustomerService,
    EmployeeService,
    AppConfig,
  ],

  bootstrap: [AppComponent]
})

export class AppModule { }
